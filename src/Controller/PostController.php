<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PostRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Post;

class PostController extends AbstractController
{
    /**
     * @Route("/posts", name="postsList")
     */
    public function index(PostRepository $postRepo): Response
    {

        $posts = $postRepo->findAll();

        /* var_dump($posts); */

        return $this->render('post/post-list.html.twig', [
            'controller_name' => 'PostController',
            'posts' => $posts
        ]);
    }
    /**
     * @Route("/posts/create", name="postCreate")
     */
    public function create(ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $post = new Post();
        $post->setTitle('hello world');
        $post->setContent('Lewangoalswki !!!!!!!!!');
        $post->setAuthor('John Doe');

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($post);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        $this->addFlash(
            'info',
            'Saved new post with id ' . $post->getId()
        );
        return $this->render('post/post-edit.html.twig', [
            'controller_name' => 'PostController'
        ]);
    }
    /**
     * @Route("/post/show/{id}", name="postShow")
     */
    public function show(int $id, PostRepository $postRepository): Response
    {
        $post = $postRepository
            ->find($id);

        return $this->render('post/post-show.html.twig', [
            'post' => $post
        ]);
    }
}

